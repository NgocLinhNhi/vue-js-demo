/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false,

  theme: {
    extend: {
      color:{
        dark:"#363853",
        primary:"#0012FF",
        green:"#67D4CA",
        red:"#F28080",
      }
    },
  },

  variants:{
    extend:{},
  },

  plugins: [],
}

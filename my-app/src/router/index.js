import {createRouter, createWebHistory} from 'vue-router'
import HomeView from '../views/index.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: ()=>import("../views/index.vue") // tên trang .vue
  },

  {
    path: '/register', // đường dẫn url gõ
    name: 'Register',
    meta: {
      layout: "auth", // tên trang .vue
    },
    component: () => import('../views/register.vue')
  },

  {
    path: '/login', // đường dẫn url gõ
    name: 'Login',
    meta: {
      layout: "auth", // tên trang .vue
    },
    component: () => import('../views/login.vue')
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router

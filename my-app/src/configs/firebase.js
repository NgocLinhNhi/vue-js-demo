import firebase from 'firebase/compat/app';
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAmEt1zhR0U8dV2lOS1JXydfxmBJZDP6ZM",
  authDomain: "vue-js-db-9ec91.firebaseapp.com",
  databaseURL: "https://vue-js-db-9ec91-default-rtdb.firebaseio.com",
  projectId: "vue-js-db-9ec91",
  storageBucket: "vue-js-db-9ec91.appspot.com",
  messagingSenderId: "708766509654",
  appId: "1:708766509654:web:ff4013fe1f6504b31d69f7",
  measurementId: "G-5D7R7FF4ZG"
};



firebase.initializeApp(firebaseConfig);

const projectAuth = firebase.auth;
const projectFireStore = firebase.firestore;

//export các config trên .
export {projectAuth, projectFireStore};



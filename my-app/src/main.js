import { createApp } from 'vue';
import App from './App.vue';
import router from './router';

import './assets/main.css';
import './assets/style/tailwind.css';
import './assets/style/global.css';

import { registerGlobalComponents} from "./utils/import"

const app = createApp(App)

//extend app vào hàm được khởi tạo component
registerGlobalComponents(app);

app.use(router)
app.mount('#app')

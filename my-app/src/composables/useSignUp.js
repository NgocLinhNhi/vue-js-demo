import {ref} from "vue";
import {projectAuth} from "@/configs/firebase";

const error = ref(null);
const isPending = ref(false);

async function signup(email, password) {
  isPending.value = true;
  error.value = null;

  try {
    debugger;
    const response = await projectAuth.createUserWithEmailAndPassword(
      email,
      password
    );
    if (!response) throw new Error("Could not create a new user.");
    await response.user.updateProfile({displayName: fullName});
    return response
  } catch (e) {
    console.log(e);
    error.value = e.message;
  } finally {
    isPending.value = false;
  }

}

export function useSignUp() {
  return {error, isPending, signup}
}
